# -*- coding: utf-8 -*-
"""
Created on Thu Oct 29 15:45:44 2020

@author: utente
"""



import serial
import tkinter as tk

MORSE_CODE_DICT = { 'A':'.-', 'B':'-...', 
                    'C':'-.-.', 'D':'-..', 'E':'.', 
                    'F':'..-.', 'G':'--.', 'H':'....', 
                    'I':'..', 'J':'.---', 'K':'-.-', 
                    'L':'.-..', 'M':'--', 'N':'-.', 
                    'O':'---', 'P':'.--.', 'Q':'--.-', 
                    'R':'.-.', 'S':'...', 'T':'-', 
                    'U':'..-', 'V':'...-', 'W':'.--', 
                    'X':'-..-', 'Y':'-.--', 'Z':'--..', 
                    '1':'.----', '2':'..---', '3':'...--', 
                    '4':'....-', '5':'.....', '6':'-....', 
                    '7':'--...', '8':'---..', '9':'----.', 
                    '0':'-----', ', ':'--..--', '.':'.-.-.-', 
                    '?':'..--..', '/':'-..-.', '-':'-....-', 
                    '(':'-.--.', ')':'-.--.-', ' ':' '} 

converted =[]
convertedString=''
txt=''
finalString=''
ser = serial.Serial('COM4',9600)

def translate():
    global convertedString    
    inputString=textEnter.get()    
    inputString=inputString.upper()
    
    for character in inputString:
        morseCharacter = MORSE_CODE_DICT[character]     
        converted.append(morseCharacter)
        
    convertedText['text']= converted
    convertedString=''.join(converted)
    
    #SEND INT INSTEAD OF DOTS AND LINES
    convertedString = convertedString.replace(".","0")
    convertedString = convertedString.replace("-","1")
    convertedString = convertedString.replace(" ","3")
    

def send():
    global convertedString    
    print(bytes(convertedString, 'UTF-8'))
    ser.write(bytes(convertedString, 'UTF-8'))
    convertedString=""
    convertedText['text']=''
    textEnter.delete(0,"end")
    textEnter.insert(0,"Write Here!")
    
def close():
    ser.close();

#GUI SETTINNGS
window = tk.Tk()
textEnter = tk.Entry(window, text ="Write here!")
convertedText = tk.Label(window, text ="")
convertButton = tk.Button(window,text="CONVERT!", command=translate)
sendButton = tk.Button(window, text= "SEND!", command = send)
closeSerial = tk.Button(window, text= "closeCom", command = close)


closeSerial.grid(row=4,column=0)
sendButton.grid(row=3,column=0)
convertButton.grid(row=2, column=0)
convertedText.grid(row=1,column =0)
textEnter.grid(row=0, column=0)
window.mainloop();


