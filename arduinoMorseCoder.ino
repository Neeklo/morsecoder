
String incoming = "";
int incomingByte;

void setup() {
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {

  if (Serial.available() > 0){
    incoming =Serial.readString();
    for(int incomingByte =0; incomingByte < incoming.length(); incomingByte++){
      char value = incoming.charAt(incomingByte);
      Serial.print(value);
      if(value=='0'){
        digitalWrite(LED_BUILTIN, HIGH); 
        delay(100);
      }
      if(value=='1'){
        digitalWrite(LED_BUILTIN, HIGH); 
        delay(300);
      }
      if(value=='3'){
        delay(600);
      }
      digitalWrite(LED_BUILTIN, LOW); 
      delay(100);
    }
  }
 Serial.flush();
}
